import tensorflow as tf
import numpy as np
from embeddings import Embeddings
"""
Here we first load the data and than train a simple LSTM network, which is initialized with the embedding layer.
"""
if __name__ == "__main__":

    # here we can choose the embedding dimmension 50/100/200/300
    emb = Embeddings(100)

    train_data, train_labels, input_shape = emb.load_toy_train_data()
    #test_data, test_labels = emb.load_toy_test_data()

    embedding_matrix, embedded_train, vocab_size = emb.prepare_data(
        train_data, input_shape)

    max_seq_length = 100

    # Define a model here
    inputs = tf.keras.layers.Input(shape=(input_shape,))

    embeded = tf.keras.layers.Embedding(vocab_size, 100, weights=[
        embedding_matrix], input_length=max_seq_length, trainable=True)(inputs)

    hidden = tf.keras.layers.LSTM(100)(embeded)
    hidden = tf.keras.layers.Dropout(0.2)(hidden)
    hidden = tf.keras.layers.Dense(10, activation='relu')(hidden)
    output = tf.keras.layers.Dense(1, activation='sigmoid')(hidden)

    model = tf.keras.Model(inputs=inputs, outputs=output)

    model.compile(optimizer='adam',
                  loss='binary_crossentropy', metrics=['acc'])
    # fit the model
    model.fit(embedded_train, train_labels, epochs=100, verbose=0)
    # evaluate the model
    loss, accuracy = model.evaluate(embedded_train, train_labels, verbose=0)

    print(f'Test accuracy: {accuracy*100}%')

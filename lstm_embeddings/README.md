This is the implementation of word embedding inicialization for LSTM neural network. The word embeddings that are used here are taken form the GloVe model (see https://nlp.stanford.edu/projects/glove/ for more detail).

To run the module and see results on the toy dataset that is also included simply run: python3 toy_embedded_lstm.py 
Beware that the script downloads the dataset from the web so you should have internet connection and approximately 800 Mb of free space on dist in order to store the embeddings. See comments inside the scripts for more detailed explanation of the functionality.
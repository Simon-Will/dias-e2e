#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def visualize_matrix(matrix, row_labels, col_labels, title=None):
    fig = plt.figure()
    ax = fig.add_subplot(111)

    mat = ax.matshow(matrix, interpolation='nearest')
    fig.colorbar(mat)

    # Dummy element at beginning because matshow is strange.
    dummy = ['']
    ax.set_xticklabels(dummy + col_labels, rotation='vertical')
    ax.set_yticklabels(dummy + row_labels)

    if title:
        ax.set_title(title)

    return fig


def visualize_probability_dist(dist, x_labels, title=None):
    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.bar(range(len(dist)), dist, align='center')

    dummy = ['']
    ax.set_xticklabels(dummy + x_labels)

    if title:
        ax.set_title(title)

    return fig

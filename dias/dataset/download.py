#!/usr/bin/python3
from requests import get
import sys
import os
import zipfile

DATASET_URL = "http://nlp.stanford.edu/projects/kvret/kvret_dataset_public.zip"
DATASET_PATH = f"{sys.path[0]}/kvret_dataset_public.zip"
DATASET_DIR = f"{sys.path[0]}"
DATASET_TEST_FILE = f"{sys.path[0]}/kvret_test_public.json"


def download_and_unzip_datasets():
    if not os.path.isfile(DATASET_PATH):
        with open(DATASET_PATH, "wb") as file:
            response = get(DATASET_URL)
            if response.status_code != 200:
                raise Exception(f"Downloading the dataset failed, status code: {response.status_code}")
            file.write(response.content)

    # The .zip file is present, but not unzipped.
    if not os.path.isfile(DATASET_TEST_FILE):
        with zipfile.ZipFile(DATASET_PATH, "r") as file:
            file.extractall(DATASET_DIR)

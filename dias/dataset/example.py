#!/usr/bin/python3

from dataset import DatasetWrapper

# An example how to obtain a Dataset containing dialogues parsed from .json files.

# During the construction of the wrapper object, dataset will be downloaded and unzipped if necessary, and parsed. 
wrapper = DatasetWrapper()

# Get test dataset. All datasets are tf.data.Dataset objects.
test_dataset = wrapper.get_test_dataset()
print(f"Type of the dataset object: {type(test_dataset)}")

vectorized_dialogue = []
# Get first vectorized dialogue from the dataset.
for dialogue in test_dataset:
    vectorized_dialogue = dialogue
    print(f"First dialogue: {vectorized_dialogue}")
    break

sentence = ""
dictionary = wrapper.get_vector_dictionary()
# Translate the dialogue back to string.
for integer in vectorized_dialogue.numpy():
    sentence += dictionary[integer] + " "

print(f"Translated dialogue: {sentence}")

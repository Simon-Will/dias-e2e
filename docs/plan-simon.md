# Project plan for reimplementing end-to-end task oriented dialogue system

## Design Priorities

We want to focus on the part of the paper where information is
extracted from the knowledge base. The encoding part (i.e. the NLU)
and the decoding part (i.e. the NLG) are not top priority. However, it
might be difficult to design a mock-up for the NLU in such a way that
it can serve as a decent proxy for an actual NLU and building a basic
RNN encoder is probably easier and more useful in the end. That’s why
the NLU should be part of the first design.

The RNN decoder for the NLG is a nice-to-have. In fact, it is easier
both to evaluate our system and to produce artificial data for it if
our system output is only an abstract representation of what the
system is supposed to say.

In the paper, the dialogue management is strongly coupled to the
knowledge base querying. It is tempting to introduce a new border at
which we could evaluate our system by splitting the dialogue
management from the knowledge base querying. But starting out like
this would mean that we would have to restructure our system
later. Instead, we should directly build the dialogue management and
evaluate it. If we have problems getting it to work and suspect that
the dialogue management is the reason, we can still split it off,
afterwards.

The paper uses a joint NLL and RL loss for optimizing the network. We
will restrict ourselves to the NLL loss and only implement the RL if
we still have some time in the end or if we have a strong cue that it
will help to improve the system significantly.

## General Considerations

The project management is mainly taking place in GitLab and makes use
of issues, milestones and boards. Slack is used for quick questions,
but if important things are discussed in Slack, the quintessence of
the discussion should be written down in the corresponding GitLab
issue.

In principle, the issues for the tasks below could all be created at
the beginning of the project. However, as it is to be expected that
the scope of the project will change a little over time, it is better
to only create the issues for current milestone. The project manager
is supposed to set this up in GitLab and each week in the lab, we can
spend around 10 minutes on a short session where we briefly report on
what we did and what task we would like to do next (sort of like a
scrum meeting).

The plan below lists the tasks in more detail. It is getting less
detailed in the end, which can be adjusted on the go.

## Detailed Plan

### Milestone I: First “Working” System

#### Until 2019-04-10

- Understand the paper https://arxiv.org/abs/1806.04441. Especially the KB attention. (6h, top priority)

- Prepare the collaboration setup and guidelines (middle priority):
  - Git repository with CI, READMES, Contribution guidelines (2h)
  - Setup slack (30m)

- Write project plans (2h, high priority)

#### Until 2019-04-24

- Write the basic training loop (2h, top priority)
  - The ML framework should be decided upon in class at 2019-04-10.

- Write toy agent that always outputs some useless stuff, but
  implements the correct interface (2h, middle priority)
  - The interface, especially what the output should look like should be decided upon in class at 2019-04-10

- Write evaluation code (2h, middle priority)
  - Metrics should be decided upon in class at 2019-04-10

- Write unit tests for the evaluation code (1h, nice to have)

- Generate artificial data (5h, high priority)

- Load [KWRET
  dataset](https://nlp.stanford.edu/blog/a-new-multi-turn-multi-domain-task-oriented-dialogue-dataset/)
  so it can be used later in training pipeline (2h, middle priority)

### Milestone II: Implementation basically done

#### Until 2019-05-01

- Write encoder (6h, high priority)
  - This includes writing stuff like vocabulary managment, batch padding, etc.
  - Should provide `h^ENC`
  - Vocabulary, batching, etc. can be split off so that two people can work on this together

- Write attention (6h, top priority)
  - Way of bringing the knowledge base into the interface should be decided upon at 2019-04-24
  - Should provide `U^KB`
  - Difficult to split into independent smaller tasks, but good opportunity for pair programming

- Write output layer and connect the loose pieces (3h, high priority)
  - The result should be an agent that can run in the training loop

#### Until 2019-05-08

- Planned buffer time and finish what was left over from last week (top priority)

- Set up nice training that prints metrics, import parameters, etc. (4h, high priority)
  - Consider tensorboard or similar tool.

- Write unit tests for batching (2h, middle priority)

- Write unit tests for various network steps (4h, nice to have)

- Use Python’s logging module or similar to introduce verbosity levels for the output (3h, nice to have)

### Milestone III: Training proven to work

#### Until 2019-05-22

- Set up system in a way that every training run creates its own directory (3h, high priority)

- Train the system on artificial data (15h, top priority)
  - Difficult to plan in more detail
  - Artificial data and network parameters should be kept small in order to ensure fast runs

- Write a script to generate plots from log files (2h, middle priority)

- Find and fix bugs in the system
  - Interleaved with previous task

### Milestone IV: System trained on real data

#### Until 2019-06-05

- Train the system on the real data (15h, top priority)

### Milestone V: Written Report

#### Until 2019-06-26

- Write about related work (6h, middle priority)

- Write model description (4h, top priority)

- Write about experiments (5h, top priority)

- Write discussion (4h, high priority)

- Write introduction conclusion (4h, middle priority)

- Clean up code (4h, middle priority)
